package fr.greta.formation.dglv.patisserie_webService;

import java.io.Serializable;

import javax.activation.DataHandler;

public class Utensils implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	private String name;
	private int size;
	private DataHandler img;
	
	public DataHandler getImg() {
		return img;
	}

	public void setImg(DataHandler img) {
		this.img = img;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	//@XmlTransient
	public int getSize() {
		return size;
	}
	
	public void setSize(int size) {
		this.size = size;
	}
	
	public Utensils(String name, int size, DataHandler img) {
		this.name = name;
		this.size = size;
		this.img = img;
		
	}
	
	public Utensils() {
		this.name = "";
		this.size = 0;
		this.img = null;
	}

	@Override
	public String toString() {
		return "Utensils [name=" + name + ", size=" + size + ", img=" + img + "]";
	}
	
	
	
}
