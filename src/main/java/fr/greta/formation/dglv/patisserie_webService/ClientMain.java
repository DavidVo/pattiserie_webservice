package fr.greta.formation.dglv.patisserie_webService;

import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;

public class ClientMain {

	public static void main(String[] args) {
		
		JaxWsProxyFactoryBean f = new JaxWsProxyFactoryBean();
		f.setAddress("http://localhost:8083/Patisserie");
		f.setServiceClass(PatisserieInterface.class);
		f.getOutInterceptors().add(new LoggingOutInterceptor());
		f.getInInterceptors().add(new LoggingInInterceptor());
		
		
		PatisserieInterface p = (PatisserieInterface) f.create();
		
		System.out.println(p.isAvailable("Poire"));
		
		for(Utensils ea:p.getUtensils()) {
			System.out.print(ea.getName() + " ");
			System.out.print(ea.getSize() + "mm ");
			System.out.println(ea.getImg().getContentType() + "\n");
		}
		
	}

}
