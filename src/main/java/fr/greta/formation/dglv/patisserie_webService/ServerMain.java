package fr.greta.formation.dglv.patisserie_webService;

import javax.xml.ws.Endpoint;
import javax.xml.ws.soap.SOAPBinding;

public class ServerMain {

	public static void main(String[] args) {
		
		// Endpoint.publish("http://localhost:8083/Patisserie", new Patisserie()); Forme la plus simple
		
		// Pour travailler avec mtom il faut:
		Endpoint ep= Endpoint.create(new Patisserie());
		ep.publish("http://localhost:8083/Patisserie");
		SOAPBinding sb=(SOAPBinding) ep.getBinding();
		sb.setMTOMEnabled(true);
	}

}
