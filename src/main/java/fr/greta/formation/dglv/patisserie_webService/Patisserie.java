package fr.greta.formation.dglv.patisserie_webService;

import java.time.LocalDate;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.jws.WebService;

@WebService
public class Patisserie implements PatisserieInterface{
	
	private String error = null;
	
	@Override
	public String getError() {
		return error;
	}
	
	@Override
	public boolean isAvailable(String obj) {
		
		if(obj.length()<=0) {
			error = "Something went wrong";
			throw new IllegalArgumentException("Something went wrong"); 
			}
		
		Boolean resp = false;
		
		int dateMonth = LocalDate.now().getMonthValue();
		
		if(obj.equals("Banane") && dateMonth >= 1 && dateMonth <= 3) resp = true;
		if(obj.equals("Poire") && dateMonth >= 4 && dateMonth <= 6) resp = true;
		if(obj.equals("Abricot") && dateMonth >= 5 && dateMonth <= 9) resp = true;
		if(obj.equals("Cerise") && dateMonth >= 10 && dateMonth <= 12) resp = true;
		
		return resp;
	}
	
	@Override
	public boolean isAvailable(String obj, String pwd) {
		if(!pwd.contentEquals("vgd")) {
			throw new SecurityException("wrong password");
		}
		return isAvailable(obj);	
	}
	
	@Override
	public Utensils[] getUtensils() {
		return new Utensils[] {
			new Utensils("Fourchette", 150, new DataHandler(new FileDataSource("img/Fourchette.jpg"))),
			new Utensils("Fouet", 170, new DataHandler(new FileDataSource("img/Fouet.jpg"))),
			new Utensils("Couteau", 250, new DataHandler(new FileDataSource("img/Couteau.jpg"))),
			new Utensils("Rouleau", 400, new DataHandler(new FileDataSource("img/Rouleau.jpg"))),
		};
	}
	
}
