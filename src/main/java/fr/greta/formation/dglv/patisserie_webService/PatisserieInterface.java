package fr.greta.formation.dglv.patisserie_webService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface PatisserieInterface {
	
	public String getError();
	
	public boolean isAvailable(String obj);
	
	@WebMethod(operationName = "isAvailableSecure")
	public boolean isAvailable(String obj, @WebParam(header=true, name="pwd") String pwd);
	
	public Utensils[] getUtensils();
}
