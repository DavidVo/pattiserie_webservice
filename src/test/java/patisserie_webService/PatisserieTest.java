package patisserie_webService;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fr.greta.formation.dglv.patisserie_webService.Patisserie;

public class PatisserieTest {
	
	@Test
	public void fruitavailablility() {
		Patisserie p=new Patisserie();
		assertEquals(false, p.isAvailable("Cerise"));
		assertEquals(true, p.isAvailable("Poire"));
		assertEquals(false, p.isAvailable("Ananas"));
	}
	
	@Test
	public void testGetError() {
		Patisserie p=new Patisserie();
		assertEquals(null, p.getError());
	}
	
	

}
